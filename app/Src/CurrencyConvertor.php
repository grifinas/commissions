<?php

namespace App\Src;

class CurrencyConvertor
{
	private $currencyConversion = [];
	private $availableCurrencies = [];
	// private $moneyConversion = [];

	public function __construct()
	{
		$this->availableCurrencies = config('commission.currency');
		
		foreach (array_keys($this->availableCurrencies) as $cur1) {
			//will make toEur, toUsd, toJpy etc.
			$this->currencyConversion[] = 'to' . ucwords(strtolower($cur1));
			// foreach (array_keys($availableCurrencies) as $cur2) {
			// 	if ($cur1 == $cur2) {
			// 		continue;
			// 	}
			// 	$this->moneyConversion[] = strtolower($cur1) . 'To' . ucwords(strtolower($cur2));
			// }
		}
	}

	public function __call($name, $args) : Currency
	{
		if (in_array($name, $this->currencyConversion)) {
			$currency = array_get($args, 0, null);
			if (!$currency instanceof Currency) {
				throw new \Exception("$name() takes Currency as first param", 2);
			}
			$data = $currency->getCurrencyInfo();

			$inEur = $currency->getAmount() / $data['to_eur'];

			$reqName = strtoupper(str_after($name, 'to'));
			$reqCurrency = $this->availableCurrencies[$reqName];

			$result = new Currency($reqName);

			$amount = $inEur * $reqCurrency['to_eur'];

			if ($data['cents'] && !$reqCurrency['cents']) {
				$amount /= 100;
			} else if (!$data['cents'] && $reqCurrency['cents']) {
				$amount *= 100;
			}

			$result->setAmount($amount);
			return $result;
		}

		throw new \Exception("Undefined method $name", 1);
	}

	public function eurToCurrency(float $amount, Currency $currency) : float
	{
		$info = $currency->getCurrencyInfo();
		if (!$info['cents']) {
			$amount /= 100;
		}
		return $amount * $info['to_eur'];
	}
}