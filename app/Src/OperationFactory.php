<?php

namespace App\Src;

use App\Src\Operations\CashIn;
use App\Src\Operations\CashOut;

class OperationFactory
{
    public static function makeOperationFromName(string $name)
    {
        $operation = null;

        switch ($name) {
            case "cash_in":
                $operation = new CashIn();
                break;
            case "cash_out":
                $operation = new CashOut();
                break;
            default:
                throw new \Exception("Operation type '{$name}' not found", 1);
        }

        return $operation;
    }
}
