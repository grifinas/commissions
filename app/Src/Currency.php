<?php

namespace App\Src;

class Currency
{
    private $currency;
    private $amount = 0;

    public function __construct(string $currency)
    {
        $availableCurencies = config('commission.currency');

        if (!isset($availableCurencies[$currency])) {
            throw new \Exception("Currency '{$currency}' not found", 1);
        }

        $this->currency = $availableCurencies[$currency];
        $this->currency['name'] = $currency;
    }

    public function getCurrencyInfo()
    {
        return $this->currency;
    }

    public function getAmount() : int
    {
        return (int)$this->amount;
    }

    public function setAmount(float $amount)
    {
        //Couldnt find propper round so i made one 
        $decimal = $amount - (int)$amount;
        $this->amount = floor($amount) + (int)($decimal*10 > 1 ? 1 : 0);
    }

    public function setAmountFloating(float $amount)
    {
        if ($this->currency['cents']) {
            $amount *= 100;
        }
        $this->amount = (int) $amount;
    }

    public function __toString()
    {
        $amount = ceil($this->amount);
        $decimalPoint = 0;
        if ($this->currency['cents']) {
            $amount = $amount / 100;
            $decimalPoint = 2;
        }
        return number_format($amount, $decimalPoint, '.', '');
    }

    public function hasCents()
    {
        return (bool)$this->currency['cents'];
    }
}
