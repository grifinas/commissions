<?php

namespace App\Src\Operations;

use App\Src\Currency;
use App\Src\CurrencyConvertor;
use App\Src\Operations\BaseOperation;
use App\Src\Operations\ExecutableOperation;
use App\Src\User;
use Carbon\Carbon;

class CashIn extends BaseOperation implements ExecutableOperation
{
    public function __construct()
    {
        $this->type = 'cash_in';
    }

    public function execute(User $user, Currency $currency, Carbon $date) : Currency
    {
        $this->setCurrency($currency);
        $convertor = new CurrencyConvertor();

        $commissionConfig = config('commission.operation.cash_in');

        $max = (int) $convertor->eurToCurrency($commissionConfig['max'], $currency) * 100;
        $min = (int) $convertor->eurToCurrency($commissionConfig['min'], $currency) * 100;

        $amount = min(max($currency->getAmount() * $commissionConfig['commission'], $min), $max);
        
        $currency->setAmount($amount);
        return $currency;
    }
}
