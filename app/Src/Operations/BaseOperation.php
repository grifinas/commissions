<?php

namespace App\Src\Operations;

use App\Src\Currency;


abstract class BaseOperation
{
    protected $type;
    private $currency;

    public function setCurrency(Currency $currency)
    {
        $this->currency = clone $currency;
    }

    public function getCurrency()
    {
        return $this->currency;
    }
}
