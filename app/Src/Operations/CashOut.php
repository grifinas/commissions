<?php

namespace App\Src\Operations;

use App\Src\Currency;
use App\Src\CurrencyConvertor;
use App\Src\Operations\BaseOperation;
use App\Src\Operations\ExecutableOperation;
use App\Src\User;
use Carbon\Carbon;

class CashOut extends BaseOperation implements ExecutableOperation
{
    public function __construct()
    {
        $this->type = 'cash_out';
    }

    public function execute(User $user, Currency $currency, Carbon $date): Currency
    {
        $this->setCurrency($currency);
        if ($user->getType() == 'legal') {
            return $this->executeLegalCashOut($currency);
        }

        $convertor = new CurrencyConvertor();

        $commissionConfig = config('commission.operation.cash_out_natural');

        $withdraws = 0;
        $totalWithdrawnCents = -1 * $commissionConfig['free_eur_per_week'] * 100;
        $operations = $user->getOperationsForWeek($date);

        if (!empty($operations)) {
            foreach ($operations as $op) {
                //Only interested in cash_out operations
                if ($op->type !== $this->type) {
                    continue;
                }

                $opcur = $op->getCurrency();

                $totalWithdrawnCents += $convertor->toEur($opcur)->getAmount();
                if (++$withdraws >= $commissionConfig['free_withdraw_per_week']) {
                    $totalWithdrawnCents = 0;
                    break;
                }
            }
        }

        //cant exceed 0 at this point
        $totalWithdrawnCents = min($totalWithdrawnCents, 0);

        $totalWithdrawnCents += $convertor->toEur($currency)->getAmount();

        //total is within the free margin of the week
        if ($totalWithdrawnCents <= 0) {
            $currency->setAmount(0);
            return $currency;
        }

        //total is above the free margin
        $amount = $totalWithdrawnCents * $commissionConfig['commission'];

        $amount = $convertor->eurToCurrency($amount, $currency);
        $currency->setAmount($amount);
        return $currency;
    }

    private function executeLegalCashOut(Currency $currency): Currency
    {
        $commissionConfig = config('commission.operation.cash_out_legal');

        $convertor = new CurrencyConvertor();

        $min = (int) $convertor->eurToCurrency($commissionConfig['min'], $currency) * 100;
        $amount = min($currency->getAmount() * $commissionConfig['commission'], $min);

        $currency->setAmount($amount);
        return $currency;
    }
}
