<?php

namespace App\Src\Operations;

use App\Src\Currency;
use App\Src\User;
use Carbon\Carbon;

interface ExecutableOperation
{
    public function execute(User $user, Currency $currency, Carbon $date);
}
