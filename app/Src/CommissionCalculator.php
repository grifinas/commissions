<?php

namespace App\Src;

use App\Src\Currency;
use App\Src\OperationFactory;
use App\Src\Operations\ExecutableOperation;
use App\Src\User;
use Carbon\Carbon;

class CommissionCalculator
{
    public function calculateCommissions(array $rows)
    {
        $users = [];
        foreach ($rows as $row) {
            $row = trim($row);
            list($date, $uid, $userType, $opType, $sum, $currency) = explode(',', $row);

            if (!isset($users[$uid])) {
                $users[$uid] = new User($uid, $userType);
            }

            $op = OperationFactory::makeOperationFromName($opType);
            $currency = new Currency($currency);
            $currency->setAmountFloating($sum);

            $date = Carbon::parse($date);

            yield $this->executeOperation($op, $users[$uid], $currency, $date);
        }
    }

    private function executeOperation(ExecutableOperation $operation, User $user, Currency $currency, Carbon $date): Currency
    {
        $response = $operation->execute($user, $currency, $date);
        $user->pushOperation($date, $operation);
        return $response;
    }
}
