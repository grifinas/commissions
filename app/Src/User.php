<?php

namespace App\Src;

use App\Src\Operations\BaseOperation;
use Carbon\Carbon;

class User
{
    private $uid;
    private $type;
    private $operations;

    public function __construct(int $uid, string $type)
    {
        $this->$uid = $uid;
        $availableTypes = config('commission.user.types');

        if (!in_array($type, $availableTypes)) {
            throw new \Exception("User type '{$type}' not found", 1);
        }

        $this->type = $type;
    }

    public function getType()
    {
        return $this->type;
    }

    public function pushOperation(Carbon $date, BaseOperation $operation)
    {
        $woy = $date->weekOfYear;
        if (!isset($this->operations[$woy])) {
            $this->operations[$woy] = [];
        }
        $this->operations[$woy][] = $operation;
    }

    public function getOperationsForWeek(Carbon $date)
    {
        return array_get($this->operations, $date->weekOfYear, []);
    }
}
