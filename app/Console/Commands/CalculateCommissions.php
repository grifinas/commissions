<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Src\CommissionCalculator;

class CalculateCommissions extends Command
{
    protected $signature = 'cli:calc-com {file}';

    protected $description = 'As input takes path to input data .csv file and calculates commissions for every entry';

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //i'd be better to use some CSV lib here, but explode is fun
        $file = file_get_contents($this->argument('file'));

        $calculator = new CommissionCalculator();

        foreach ($calculator->calculateCommissions(explode("\n", $file)) as $row) {
            echo $row . PHP_EOL;
        }
    }
}
