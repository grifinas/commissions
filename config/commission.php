<?php

return [
	"currency" => [
		"EUR" => [
			"to_eur" => 1,
			"cents" => 1,
		], 
		"USD" => [
			"to_eur" => 1.1497,
			"cents" => 1,
		], 
		"JPY" => [
			"to_eur" => 129.53,
			"cents" => 0,
		], 
	],
	"user" => [
		"types" => ["natural", "legal"],
	],
	"operation" => [
		"cash_in" => [
			"commission" => 0.0003,
			"min" => 0,
			"max" => 5,
		],
		"cash_out_legal" => [
			"commission" => 0.003,
			"min" => 5,
		],
		"cash_out_natural" => [
			"free_eur_per_week" => 1000,
			"free_withdraw_per_week" => 3,
			"min" => 0,
			"commission" => 0.003,
		],
	],
];