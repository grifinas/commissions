<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\Src\CommissionCalculator;

class CommissionCalculationTest extends TestCase
{
    public function testProvidedInputFile()
    {
        $file = file_get_contents('tests/input.csv');
        $answers = file_get_contents('tests/answers.csv');
        $answers = explode("\n", $answers);

        $calculator = new CommissionCalculator();
        $line = 0;

        foreach ($calculator->calculateCommissions(explode("\n", $file)) as $row) {
            $this->assertEquals((string)$row, trim($answers[$line++]));
        }
    }
}
